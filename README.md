# Network DevOps Training at ENSA Marrakech GRT

## Lab Setup

### Install EVE-NG

1. [EVE-NG Website](https://www.eve-ng.net/)
2. [EVE-NG Community Edition Download](https://www.eve-ng.net/index.php/download/#DL-COMM)
3. [EVE-NG Installation](https://www.eve-ng.net/index.php/documentation/installation/virtual-machine-install/)

#### Add Cisco images

1. [Add Cisco Dynamips images - Cisco IOS](https://www.eve-ng.net/index.php/documentation/howtos/howto-add-cisco-dynamips-images-cisco-ios/)
2. [Add Cisco vIOS from VIRL](https://www.eve-ng.net/index.php/documentation/howtos/howto-add-cisco-vios-from-virl/)
3. [Add Cisco XRv](https://www.eve-ng.net/index.php/documentation/howtos/howto-add-cisco-xrv/)

#### Add Ubuntu 16.04 Server

1. [Add Linux](https://www.eve-ng.net/index.php/documentation/howtos/howto-create-own-linux-host-image/)
2. [Add Windows](https://www.eve-ng.net/index.php/documentation/howtos/howto-create-own-windows-host-on-the-eve/)

#### Enable Internet Access

1. [Internet Access on VMware](https://docs.vmware.com/en/VMware-Workstation-Pro/15.0/com.vmware.ws.using.doc/GUID-4D35D1FC-5926-45A8-96B1-42C922DA97E9.html)

2. [Internet Access on EVE-NG](https://learningnetwork.cisco.com/docs/DOC-37068)
3. Internet Access on Cisco Routers (static route to VMware0 adapter) : `ip addr 0.0.0.0 0.0.0.0 <NEXT_HOP = VMware GATEWAY>`
4. Internet Access on Ubuntu 16.04 is already enabled since DHCP is configured by default.

#### Important notes on using Gitlab CI/CD

1. While learning CI/CD on Gitlab.com, your local Ansible server must be accessible from the public Internet so you can push configuration to your local network lab. Use [ngrok](https://www.ngrok.com/) to get a Public URL for SSH access to your Ansible server. (use `./ngrok tcp 22` for SSH).

2. Everytime you create a NEW ngrok tunnel, don't forget the change the **randomly** generated port on `.gitlab-ci.yml` file **(Line 24)**
3. Configure `SSH_PRIVATE_KEY` environment variable on Gitlab ENV Variables (Project > Settings > CI/CD > Variables > Expand). **Don't** enable **Protected** and **Masked** options.
4. [How To Set Up SSH Keys on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-1604)

### Initial configuration

#### Configure SSHv2 on Cisco routers

- [ios_r1](init_config/ios_r1.txt)
- [ios_r2](init_config/ios_r1.txt)

#### Configure Ubuntu 16.04 Server

##### Install Python3.7 & PIP

1. [Install Python3.7](https://stackoverflow.com/questions/51279791/how-to-upgrade-python-version-to-3-7/51280444#51280444)
2. [Install PIP for Python3.7](https://stackoverflow.com/questions/54633657/how-to-install-pip-for-python-3-7-on-ubuntu-18/56140616#56140616)
3. Map `python3` command to `python3.7` to avoid using the wrong python version : `sudo update-alternatives --install /usr/bin/python python3 /usr/bin/python3 10`
4. Verify `python3 --version` and `ansible --version` You should see Python3.7 as the default version.
 
##### Install Requirements

1. Use `sudo python3.7 -m pip install requirements.txt` to install required modules (*ansible v2.8.7*, *paramiko*, *yamllint* and *netaddr*)

## Documentation

### Ansible

- [Demo: Network Automation With Red Hat Ansible Engine For Beginners](https://www.ansible.com/resources/videos/network-automation-with-red-hat-ansible-engine-for-beginners)
- [Cisco IOS modules list](https://docs.ansible.com/ansible/latest/modules/list_of_network_modules.html#ios)
- [Cisco ISO XR modules list](https://docs.ansible.com/ansible/latest/modules/list_of_network_modules.html#iosxr)
- [Cisco ios_config module](https://docs.ansible.com/ansible/latest/modules/ios_config_module.html)
- [Ansible Books](https://www.ansible.com/resources/whitepapers)

### Git/Gitlab

- [Learn Git Version Control using Interactive Browser-Based Scenarios](https://www.katacoda.com/courses/git)
- [A Beginner’s Git and GitHub Tutorial](https://blog.udacity.com/2015/06/a-beginners-git-github-tutorial.html)
- [Git Workflow](http://csci221.artifice.cc/images/simple_git_daily_workflow.png)
- [GitLab basics guides](https://docs.gitlab.com/ee/gitlab-basics/)
- Clone (or Fork) this Gitlab project and sync it with your local repository on Ansible server (really easy just Google it 😊 )

### CI/CD

- [Introduction to CI/CD with GitLab](https://docs.gitlab.com/ee/ci/introduction/)
- [A quick guide to GitLab CI/CD pipelines](https://about.gitlab.com/blog/2019/07/12/guide-to-ci-cd-pipelines/)

## References 

- [Network DevOps by Nick Russo - O'Reilly Live Training](https://learning.oreilly.com/live-training/courses/network-devops/0636920300984/)
- [Ansible for Managing Network Devices by Nick Russo - O'Reilly Live Training](https://learning.oreilly.com/live-training/courses/ansible-for-managing-network-devices/0636920270188/)